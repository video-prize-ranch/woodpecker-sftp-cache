# Woodpecker SFTP Cache
Cache directories using SFTP.

## Usage
```
pipeline:
  restore:
    image: codeberg.org/video-prize-ranch/woodpecker-sftp-cache
    secrets: [ssh_key]
    settings:
      name: go
      path: /go
      ssh_key:
      	from_secret: ssh_key
      restore: true # Change to false to upload cache
      port: 22
      user: user
      host: 10.20.1.1
      cache_path: /cache
```
