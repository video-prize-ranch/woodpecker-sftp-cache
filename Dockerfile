FROM alpine:latest

WORKDIR /app
COPY cache.sh .

RUN apk --no-cache add zstd openssh-client-default

ENTRYPOINT /app/cache.sh
