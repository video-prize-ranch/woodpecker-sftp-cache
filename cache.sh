#!/bin/sh

echo "$PLUGIN_SSH_KEY" > /ssh_key

if [ "$PLUGIN_RESTORE" = "true" ]; then
  scp -i /ssh_key -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -P "$PLUGIN_PORT" "$PLUGIN_USER@$PLUGIN_HOST:$PLUGIN_CACHE_PATH" "$PLUGIN_NAME.tar.zst" || true
  zstd -d --stdout "$PLUGIN_NAME.tar.zst" | tar x -f - -C / || true
fi

if [ "$PLUGIN_RESTORE" = "false" ]; then
  tar c -C / -f - "$PLUGIN_PATH" | zstd -9 -o "$PLUGIN_NAME.tar.zst" || true
  scp -i /ssh_key -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -P "$PLUGIN_PORT" "$PLUGIN_NAME.tar.zst" "$PLUGIN_USER@$PLUGIN_HOST:$PLUGIN_CACHE_PATH" || true
fi
